package com.jlr.cat.importer;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by sanketsarang on 07/11/18.
 */
public class ExportMFD {

    public static void main(String[] args) throws IOException {
        Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

        Workbook workbook = new XSSFWorkbook();


        Sheet sheet = workbook.createSheet();

        sheet.createRow(0);

        sheet.getRow(0).createCell(0).setCellValue("_id");
        sheet.getRow(0).createCell(1).setCellValue("RADS ID");
        sheet.getRow(0).createCell(2).setCellValue("Jaguar Description");
        sheet.getRow(0).createCell(3).setCellValue("Land Rover Description");
        sheet.getRow(0).createCell(4).setCellValue("System Description");
        sheet.getRow(0).createCell(5).setCellValue("Applicability");
        sheet.getRow(0).createCell(6).setCellValue("Mapping");

        int rowNum = 1;

        List<JSONObject> itemList = Db.searchAsJson(Query.select().from("OxoMasterFeatureMapping"));

        for (JSONObject item : itemList) {
            final Row row = sheet.createRow(rowNum ++);
            row.createCell(0).setCellValue(item.getString("_id"));
            row.createCell(1).setCellValue(item.getString("radsId"));
            row.createCell(2).setCellValue(item.getString("jaguarDescriptions"));
            row.createCell(3).setCellValue(item.getString("landRoverDescription"));
            row.createCell(4).setCellValue(item.getString("systemDescription"));
            row.createCell(5).setCellValue(item.getString("applicability"));
            row.createCell(6).setCellValue(item.getString("exactCodes"));
        }

        FileOutputStream fos = new FileOutputStream(new File("/mnt/mfd-mappings.xlsx"));
        workbook.write(fos);

        workbook.close();
    }
}
