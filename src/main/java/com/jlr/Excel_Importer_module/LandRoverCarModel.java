package com.jlr.Excel_Importer_module;

import java.util.List;

import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class LandRoverCarModel {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");
		JsonParser parser = new JsonParser();

		List<JSONObject> masterMarkets = Db.searchAsJson(Query.select().from("MasterMarkets"));
		List<JSONObject> masterModels = Db.searchAsJson(Query.select().from("MasterModels"));

		masterMarkets.forEach(market -> {
			masterModels.forEach(model -> {
				List<JSONObject> LandRoverCarList = Db.searchAsJson(Query.select().from("CarModels")
						.where(SearchParam.create("manufacturer").eq("Land Rover")
								.and(SearchParam.create("market").eq(market.getString("market")))
								.and(SearchParam.create("model").eq(model.getString("model")))));

				if (LandRoverCarList.size() == 0)
					return;
				LandRoverCarList.forEach(car -> {

					String _id = (String) car.get("_id");
					JSONObject features = car.getJSONObject("features");
					JSONObject object = features.getJSONObject("102");

					if (!object.get("category").toString().equals("S")) {
						Db.execute("delete from `jlr-db-sb`.`CarModels` where `_id`= '" + _id + "'");

						JSONObject jsnVersion = new JSONObject();
						jsnVersion.put("price", object.getString("price"));
						jsnVersion.put("comment", object.getString("comment"));
						jsnVersion.put("category", "S");

						features.put("102", jsnVersion);

						car.remove("features");
						car.put("features", features);
						Db.insertJson("CarModels", (JsonObject) parser.parse(car.toString()));
					}
				});
			});
		});
		System.out.println("done...");
	}
}