package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by sanketsarang on 13/11/18.
 */
public class SpecDateFormat implements Validator {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");

    @Override
    public JSONObject validate(JSONObject carJson) {
        JSONObject features = carJson.getJSONObject("features");

        if(features.getJSONObject("13").getString("category").isEmpty()) {
            return new JSONObject().put("error", "Price / Spec Date cannot be blank");
        }

        try {
            sdf.parse(features.getJSONObject("13").getString("category"));
        } catch (ParseException e) {
            return new JSONObject().put("error","Price / Spec Date should be of format: dd-MMM-yy. It is currently: " + features.getJSONObject("13").getString("category"));
        }

        return new JSONObject().put("error", "");
    }
}
