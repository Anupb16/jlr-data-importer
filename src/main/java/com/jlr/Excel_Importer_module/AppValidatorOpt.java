package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.search.Query;

public class AppValidatorOpt {
	int count;

	@SuppressWarnings("unchecked")
	public void validateFiles(String file, List<JSONObject> masterMarkets, List<JSONObject> masterManufacturers,
			List<JSONObject> masterModels) throws Exception {

		String errorString = "";
		FileInputStream excelFile = new FileInputStream(new File(file));

		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);

		int lastRow = datatypeSheet.getLastRowNum();
		Row row1 = datatypeSheet.getRow(1);

		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			datatypeSheet = workbook.getSheetAt(1);
			lastRow = datatypeSheet.getLastRowNum();
			row1 = datatypeSheet.getRow(1);
			if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
				datatypeSheet = workbook.getSheetAt(2);
				lastRow = datatypeSheet.getLastRowNum();
				row1 = datatypeSheet.getRow(1);
			}
		}
		if (lastRow > 579) {
			lastRow = 579;
		} else {
			lastRow = datatypeSheet.getLastRowNum();
		}
		// 1. Validating Sheet number
		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			errorString = "File : " + file + " Benchmark Sheet is not at first position.\n";

		} else {
			// 2. Validating Feature Code

			Map<Integer, String> map = new HashMap<>();
			List<String> masterFeaturesDb = new ArrayList<>();
			List<String> masterFeaturesExcel = new ArrayList<>();

			List<JSONObject> orderedMasterFeatures = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));
			for (JSONObject jsonObject : orderedMasterFeatures) {
				map.put(Integer.valueOf(jsonObject.getString("uniqueSerialNo")), jsonObject.getString("featureCode"));
			}
			Set st = (Set) map.entrySet();
			Iterator it = st.iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Entry) it.next();
				masterFeaturesDb.add(entry.getValue().toString());
			}

			//TOOD: Gives null pointer if there are extra rows at the end. Need to check.

			for (int rn = 1; rn <= lastRow; rn++) {
				Row row = datatypeSheet.getRow(rn);
				if (row.getCell(1) != null && (!String.valueOf(row.getCell(1)).equals(""))) {
					masterFeaturesExcel.add(row.getCell(1).toString().trim());
				}
			}
			if (!masterFeaturesDb.equals(masterFeaturesExcel)) {
				errorString += "Feature code error in file: " + file + "\n";
				for(int i = 0; i < masterFeaturesDb.size(); i++) {
					if(!masterFeaturesDb.get(i).equals(masterFeaturesExcel.get(i))) {
//						errorString += "At row " + i + " found feature code " + masterFeaturesExcel.get(i) + " instead of " + masterFeaturesDb.get(i) + "\n";
					}
				}
//				errorString = errorString + " " + " File: " + file + " Feature code does not match\n";
			}

			int i = 7;

			List<String> dbMarkets = new ArrayList<>();
			for (JSONObject jsonObject : masterMarkets) {
				dbMarkets.add(jsonObject.getString("market"));
			}
			if (row1.getCell(6) != null) {
				if (row1.getCell(6).toString().trim().equalsIgnoreCase("Market")) {
					int k = i;

					while (k < datatypeSheet.getRow(4).getLastCellNum() && datatypeSheet.getRow(4).getCell(i) != null) {
						if ((String.valueOf(row1.getCell(k)) != "") && row1.getCell(k) != null) {
							if (dbMarkets.contains(row1.getCell(k).toString().trim())) {
							} else {
								errorString = errorString + " " + " File: " + file + " Market : "
										+ row1.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
							}
						} else {
							// errorString = errorString + " " + " File: " + file + " Market : "
							// + row1.getCell(k).toString().trim() + " at column " + k + " does not
							// exist\n";
						}
						k = k + 3;
					}
				}
			}

			Row row2 = datatypeSheet.getRow(2);
			List<String> dbManufacturers = new ArrayList<>();
			for (JSONObject jsonObject : masterManufacturers) {
				dbManufacturers.add(jsonObject.getString("manufacturer"));
			}
			if (row2.getCell(6) != null) {
				if (row2.getCell(6).toString().trim().equalsIgnoreCase("Manufacturer")) {
					int k = i;

					while (k < datatypeSheet.getRow(4).getLastCellNum() && datatypeSheet.getRow(4).getCell(i) != null) {
						if ((String.valueOf(row2.getCell(k)) != "") && row2.getCell(k) != null) {
							if (dbManufacturers.contains(row2.getCell(k).toString().trim())) {
								if (row2.getCell(k).toString().trim().equalsIgnoreCase("Land Rover")
										|| row2.getCell(k).toString().trim().equalsIgnoreCase("Jaguar")) { // TODO
																											// remove
																											// later
									errorString = errorString + " " + " File: " + file + " Manufacturer : "
											+ row2.getCell(k).toString().trim() + " at column " + k + " do not Add\n";
								}
							} else {
								errorString = errorString + " " + " File: " + file + " Manufacturer : "
										+ row2.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
							}
						} else {
							// errorString = errorString + " " + " File: " + file + " Manufacturer : "
							// + row2.getCell(k).toString().trim() + " at column " + k + " does not
							// exist\n";
						}
						k = k + 3;
					}
				}
			}

			Row row3 = datatypeSheet.getRow(3);
			List<String> dbModels = new ArrayList<>();
			for (JSONObject jsonObject : masterModels) {
				dbModels.add(jsonObject.getString("model"));
			}
			if (row3.getCell(6) != null) {
				if (row3.getCell(6).toString().trim().equalsIgnoreCase("Model")) {
					int k = i;
					while (k < datatypeSheet.getRow(4).getLastCellNum() && datatypeSheet.getRow(4).getCell(i) != null) {
						if ((String.valueOf(row3.getCell(k)) != "") && row3.getCell(k) != null) {
							if (dbModels.contains(row3.getCell(k).toString().trim())) {
							} else {
								if (row3.getCell(k).toString().trim().equals("E-pace")
										|| row3.getCell(k).toString().trim().equals("E-Pace")
										|| row3.getCell(k).toString().trim().equals("I-Pace")
										|| row3.getCell(k).toString().trim().equals("F-Pace")
										|| row3.getCell(k).toString().trim().equals("5 series")
										|| row3.getCell(k).toString().trim().equals("S-Class")
										|| row3.getCell(k).toString().trim().equals("Land Cruiser")
										|| row3.getCell(k).toString().trim().equals("911.0")) {
								} else {
									errorString = errorString + " " + " File: " + file + " Model : "
											+ row3.getCell(k).toString().trim() + " at column " + k
											+ " does not exist\n";
								}
							}
						} else {
							// errorString = errorString + " " + " File: " + file + " Model : "
							// + row3.getCell(k).toString().trim() + " at column " + k + " does not
							// exist\n";
						}
						k = k + 3;
					}
				}
			}

			errorString = validateCategoryRules(file, errorString, datatypeSheet, lastRow, i);
		}
		workbook.close();
		if (!errorString.equals("")) {
			sendEmail(errorString);
		}
	}

	public String validateCategoryRules(String file, String errorString, Sheet datatypeSheet, int lastRow, int i) {
		int k = i;
		while (k < datatypeSheet.getRow(4).getLastCellNum() && datatypeSheet.getRow(4).getCell(i) != null) {
			count = 0;
			RowCount c = new RowCount();
			while (c.getRowCount() <= lastRow) {
				Row row = datatypeSheet.getRow(c.getRowCount());

				int catVal = validateCategory(row, k);
				if (catVal == 2) {
					errorString = errorString + " " + " File: " + file + " at column " + k
							+ " feature code 502 and 503 has equal category S.\n";
				}
				c.incrementRowCount();
			}
			k = k + 3;
		}
		return errorString;
	}

	private int validateCategory(Row row, int i) {
		try {
			if (row != null) {
				if (row.getCell(i) != null && (String.valueOf(row.getCell(i)) != "")) {
					switch (Integer.parseInt(Long.toString(Math.round(Double.parseDouble(row.getCell(1).toString()))))) {
						case 502:
							if (row.getCell(i).toString().trim().equals("S")) {
								count++;
							}
							return count;
						case 503:
							if (row.getCell(i).toString().trim().equals("S")) {
								count++;
							}
							return count;
						default:
							return count;
					}
				}
			}
			return count;
		}catch(Exception ex) {
			System.out.println("Error at row: " + row.getRowNum() + " column: " + i);
			return 0;
		}
	}

	private void sendEmail(String errorString) {
//		System.out.println("Email...." + errorString);
//		System.out.println(" ");
	}
}