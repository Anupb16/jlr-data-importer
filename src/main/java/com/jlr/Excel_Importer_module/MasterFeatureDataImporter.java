package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlr.Excel_Importer_module.models.OrderedMasterFeatures;

public class MasterFeatureDataImporter {

	static Map<Integer, Boolean> catFeaturesMap = new HashMap<Integer, Boolean>();

	@SuppressWarnings("unchecked")
	public static void main(String args[]) throws Exception {
		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<JSONObject> catFeatures = Db.searchAsJson(Query.select().from("CatFeatures"));

		for (JSONObject jsonObject : catFeatures) {
			catFeaturesMap.put(Integer.parseInt(jsonObject.get("uniqueRefno").toString()),
					(Boolean) jsonObject.get("specAdjustable"));
		}

		List<String> filePaths = new ArrayList<String>();

		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");

		App listFiles = new App();
		filePaths = listFiles.listAllFiles(folder);
		for (String file : filePaths) {
			processFile(file);
		}
		System.out.println("done...");
	}

	public static void processFile(String filePath) throws Exception {

		OrderedMasterFeatures masterFeature = null;

		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);

		System.out.println("val.. " + datatypeSheet.getLastRowNum());
		for (int rn = 1; rn <= datatypeSheet.getLastRowNum(); rn++) {//538
			Row row = datatypeSheet.getRow(rn);
			masterFeature = new OrderedMasterFeatures();
			int uniqueSerialNo = rn;

			for (int col = 0; col <=8 ; col++) {

				if (col == 0) {
					masterFeature.setUniqueSerialNo(String.valueOf(uniqueSerialNo));
					masterFeature.setFeatureCode(row.getCell(col).toString().trim().substring(0,
							row.getCell(col).toString().trim().lastIndexOf(".")));

					if (catFeaturesMap.containsKey(Integer.parseInt(masterFeature.getFeatureCode()))) {
						masterFeature.setSpecAdjustable(catFeaturesMap.get(Integer.parseInt(masterFeature.getFeatureCode())));
					} else {
						masterFeature.setSpecAdjustable(false); // feature code 212, 544
					}
				}
				if (col == 1) {
					masterFeature.setFeatureGroup(row.getCell(col).toString().trim());
				}
				if (col == 4) {
					masterFeature.setFeatureName(row.getCell(col).toString().trim());
				}
				if (col == 7) {
					masterFeature.setMerged(row.getCell(col).getBooleanCellValue());
				}
				if (col == 8) {
					masterFeature.setVisible(row.getCell(col).getBooleanCellValue());
				}
			}
			JSONObject json = new JSONObject(masterFeature);
			JsonParser jsonParser = new JsonParser();
			JsonObject masterFeatures = (JsonObject) jsonParser.parse(json.toString());
			Db.insertJson("OrderedMasterFeatures", masterFeatures);
		}
		workbook.close();
	}
}