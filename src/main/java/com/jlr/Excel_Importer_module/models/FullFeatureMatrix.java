package com.jlr.Excel_Importer_module.models;

import com.blobcity.db.Db;
import com.blobcity.db.annotations.Entity;

@Entity(ds = "jlr-db")
public class FullFeatureMatrix extends Db {

	private String modelCode;
	private String featureGroup;
	private String jlrCode;
	private String oaCode;
	private String wersCodes;
	private String systemDescription;
	private String jaguarDescriptions;
	private String landRoverDescription;
	private String longDescription;
	private String efgCode;
	private String efgType;
	private String efgDescription;
	private String collectorCode;
	private String configurationGroup;
	private String applicability;

	public FullFeatureMatrix() {
		
	}

	public FullFeatureMatrix(String modelCode, String featureGroup, String jlrCode, String oaCode, String wersCodes, 
			String systemDescription, String jaguarDescriptions, String landRoverDescription, String longDescription, String efgCode, String efgType, String efgDescription,
			String collectorCode, String configurationGroup, String applicability)  {
		super();
		this.modelCode = modelCode;
		this.featureGroup = featureGroup;
		this.jlrCode = jlrCode;
		this.oaCode = oaCode;
		this.wersCodes = wersCodes;
		this.systemDescription = systemDescription;
		this.jaguarDescriptions = jaguarDescriptions;
		this.landRoverDescription = landRoverDescription;
		this.longDescription = longDescription;
		this.efgCode = efgCode;
		this.efgType = efgType;
		this.efgDescription = efgDescription;
		this.collectorCode = collectorCode;
		this.configurationGroup = configurationGroup;
		this.applicability = applicability;
	}

	public String getModelCode() {
		return modelCode;
	}

	public void setModelCode(String modelCode) {
		this.modelCode = modelCode;
	}

	public String getFeatureGroup() {
		return featureGroup;
	}

	public void setFeatureGroup(String featureGroup) {
		this.featureGroup = featureGroup;
	}

	public String getJlrCode() {
		return jlrCode;
	}

	public void setJlrCode(String jlrCode) {
		this.jlrCode = jlrCode;
	}

	public String getOaCode() {
		return oaCode;
	}

	public void setOaCode(String oaCode) {
		this.oaCode = oaCode;
	}

	public String getWersCodes() {
		return wersCodes;
	}

	public void setWersCodes(String wersCodes) {
		this.wersCodes = wersCodes;
	}

	public String getSystemDescription() {
		return systemDescription;
	}

	public void setSystemDescription(String systemDescription) {
		this.systemDescription = systemDescription;
	}

	public String getJaguarDescriptions() {
		return jaguarDescriptions;
	}

	public void setJaguarDescriptions(String jaguarDescriptions) {
		this.jaguarDescriptions = jaguarDescriptions;
	}

	public String getLandRoverDescription() {
		return landRoverDescription;
	}

	public void setLandRoverDescription(String landRoverDescription) {
		this.landRoverDescription = landRoverDescription;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public String getEfgCode() {
		return efgCode;
	}

	public void setEfgCode(String efgCode) {
		this.efgCode = efgCode;
	}

	public String getEfgType() {
		return efgType;
	}

	public void setEfgType(String efgType) {
		this.efgType = efgType;
	}

	public String getEfgDescription() {
		return efgDescription;
	}

	public void setEfgDescription(String efgDescription) {
		this.efgDescription = efgDescription;
	}

	public String getCollectorCode() {
		return collectorCode;
	}

	public void setCollectorCode(String collectorCode) {
		this.collectorCode = collectorCode;
	}

	public String getConfigurationGroup() {
		return configurationGroup;
	}

	public void setConfigurationGroup(String configurationGroup) {
		this.configurationGroup = configurationGroup;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}
}