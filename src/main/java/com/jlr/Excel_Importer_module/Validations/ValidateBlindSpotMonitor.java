package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateBlindSpotMonitor implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

//        if(features.getJSONObject("508").getString("category").isEmpty()) {
//            return "for Blind Spot Assist category cannot be blank";
//        }

        if(features.getJSONObject("508").getString("category").equals("S")) {
			if(!features.getJSONObject("507").getString("category").equals("NA")) {
				features.getJSONObject("507").put("category", "NA");
				carJson.put("features", features);
				return carJson;
			}
		}

        return new JSONObject().put("error", "");
	}

}
