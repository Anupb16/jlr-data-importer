package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateHighSpeedEmergencyBraking implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		
		String manufacturer = carJson.getString("manufacturer");
		JSONObject features = carJson.getJSONObject("features");
		
		if(manufacturer.equalsIgnoreCase("Jaguar") || manufacturer.equalsIgnoreCase("Land Rover")) {
			/*if(features.getJSONObject("523").getString("category").isEmpty()) {
	            return "for Adaptive Cruise Control with Stop & Go category cannot be blank";
	        }*/

	        if(features.getJSONObject("523").getString("category").equals("S")) {
				if(!features.getJSONObject("518").getString("category").equals("S")) {
					features.getJSONObject("518").put("category", "S");
					carJson.put("features", features);
					return carJson;
					//return "High-Speed Emergency Braking must be S if Adaptive Cruise Control with Stop & Go is S";
				}
			}
		}

        return new JSONObject().put("error", "");
	}

}
