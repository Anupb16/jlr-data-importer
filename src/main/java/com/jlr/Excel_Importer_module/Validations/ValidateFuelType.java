package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

/**
 * Created by sanketsarang on 05/11/18.
 */
public class ValidateFuelType implements Validator {

    @Override
    public JSONObject validate(JSONObject carJson) {
        JSONObject features = carJson.getJSONObject("features");

        if(!carJson.has("fuel")) {
            return new JSONObject().put("error", "No fuel specification found for car");
        } else {
            switch(carJson.getString("fuel")) {
                case "Diesel":
                case "Petrol":
                case "D-PHEV":
                case "P-PHEV":
                case "BEV":
                    break;
                default:
                    return new JSONObject().put("error", "Un-recognised fuel type " + carJson.getString("fuel"));
            }
        }

        return new JSONObject().put("error", "");
    }
}
