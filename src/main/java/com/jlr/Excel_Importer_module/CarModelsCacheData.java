package com.jlr.Excel_Importer_module;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;

public class CarModelsCacheData {

	private static final CarModelsCacheData instance;

	static {
		instance = new CarModelsCacheData("market", "manufacturer", "models", "trim", "modelYear", "fuel", "power", "specDate");
	}

	public static CarModelsCacheData getInstance() {
		return instance;
	}

	private final Map<String, JSONObject> models = new HashMap<>();

	@SuppressWarnings("unchecked")
	public CarModelsCacheData(String market, String manufacturer, String mod, String trim, String modelYear,
			String fuel, String power, String specDate) {
		List<JSONObject> marketList = Db.searchAsJson(Query.select().from("CarModels")
				.where(SearchParam.create("market").eq(market).and(SearchParam.create("manufacturer").eq(manufacturer))
						.and(SearchParam.create("model").eq(mod)).and(SearchParam.create("trim").eq(trim))
						.and(SearchParam.create("modelYear").eq(modelYear)).and(SearchParam.create("fuel").eq(fuel))
						.and(SearchParam.create("power").eq(power)).and(SearchParam.create("spec_date").eq(specDate))));
		marketList.forEach(model -> {
			try {
				models.put(model.getString("market") + model.getString("manufacturer") + model.getString("model")
						+ model.getString("trim") + model.getString("modelYear") + model.getString("fuel")
						+ model.getString("power") + model.getString("spec_date"), model);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		});
	}

	public JSONObject getModel(String modelName) {
		return models.get(modelName);
	}

	public boolean containsModel(String modelName) {
		return models.containsKey(modelName);
	}
}
