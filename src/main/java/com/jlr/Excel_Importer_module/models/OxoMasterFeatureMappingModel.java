package com.jlr.Excel_Importer_module.models;

import com.blobcity.db.Db;
import com.blobcity.db.annotations.Entity;

@Entity(ds = "jlr-db")
public class OxoMasterFeatureMappingModel extends Db {

	private String RadsId;
	private String featureGroup;
	private String systemDescription;
	private String jaguarDescriptions;
	private String landRoverDescription;
	private String applicability;
	private String[] exactCodes;

	public OxoMasterFeatureMappingModel() {
		
	}

	public OxoMasterFeatureMappingModel(String RadsId, String featureGroup, 
			String systemDescription, String jaguarDescriptions, String landRoverDescription, String applicability, String[] exactCodes)  {
		super();
		this.RadsId = RadsId;
		this.featureGroup = featureGroup;
		this.systemDescription = systemDescription;
		this.jaguarDescriptions = jaguarDescriptions;
		this.landRoverDescription = landRoverDescription;
		this.applicability = applicability;
		this.exactCodes = exactCodes;
	}

	public String getRadsId() {
		return RadsId;
	}

	public void setRadsId(String radsId) {
		RadsId = radsId;
	}

	public String getFeatureGroup() {
		return featureGroup;
	}

	public void setFeatureGroup(String featureGroup) {
		this.featureGroup = featureGroup;
	}

	public String getSystemDescription() {
		return systemDescription;
	}

	public void setSystemDescription(String systemDescription) {
		this.systemDescription = systemDescription;
	}

	public String getJaguarDescriptions() {
		return jaguarDescriptions;
	}

	public void setJaguarDescriptions(String jaguarDescriptions) {
		this.jaguarDescriptions = jaguarDescriptions;
	}

	public String getLandRoverDescription() {
		return landRoverDescription;
	}

	public void setLandRoverDescription(String landRoverDescription) {
		this.landRoverDescription = landRoverDescription;
	}

	public String getApplicability() {
		return applicability;
	}

	public void setApplicability(String applicability) {
		this.applicability = applicability;
	}

	public String[] getExactCodes() {
		return exactCodes;
	}

	public void setExactCodes(String[] exactCodes) {
		this.exactCodes = exactCodes;
	}
}