package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class HeadlampsMatrix implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("176").getString("category").isEmpty()) {
			return "for  Headlamps - Matrix category cannot be blank";
		}*/

		if (features.getJSONObject("176").getString("category").equals("S")) {
			if (!features.getJSONObject("181").getString("category").equals("S")
					&& !features.getJSONObject("182").getString("category").equals("S")
					&& !features.getJSONObject("183").getString("category").equals("S")
					&& !features.getJSONObject("184").getString("category").equals("S")
					&& !features.getJSONObject("187").getString("category").equals("S")) {
				features.getJSONObject("181").put("category", "S");
				features.getJSONObject("182").put("category", "S");
				features.getJSONObject("183").put("category", "S");
				features.getJSONObject("184").put("category", "S");
				features.getJSONObject("187").put("category", "S");
				
				carJson.put("features", features);
				return carJson;
				//return "Headlamps: Adaptive, Automatic, Automatic Levelling, Follow me Home Lighting and Adaptive Driving Beam must all be S if Headlamps - Matrix is S";
			}
		}

		return new JSONObject().put("error", "");
	}

}
