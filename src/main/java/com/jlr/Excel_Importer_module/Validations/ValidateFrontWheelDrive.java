package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateFrontWheelDrive implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("104").getString("category").isEmpty()) {
			return "for Rear Wheel Drive RWD category cannot be blank";
		}*/

		if (features.getJSONObject("104").getString("category").equals("S")) {
			if (!features.getJSONObject("105").getString("category").equals("NA")) {
				return new JSONObject().put("error", "Front Wheel Drive FWD must be NA if Rear Wheel Drive RWD is S");
			}
		}

		return new JSONObject().put("error", "");
	}

}
