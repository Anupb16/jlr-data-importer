package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateCoPilotDrive implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		String manufacturer = carJson.getString("manufacturer");
		JSONObject features = carJson.getJSONObject("features");
		
		if(manufacturer.equalsIgnoreCase("Jaguar") || manufacturer.equalsIgnoreCase("Land Rover")) {
			/*if(features.getJSONObject("526").getString("category").isEmpty()) {
	            return "for Co-Pilot Drive category cannot be blank";
	        }*/

	        if(features.getJSONObject("526").getString("category").equals("S")) {
				if(!features.getJSONObject("518").getString("category").equals("S")) {
					features.getJSONObject("518").put("category", "S");
					carJson.put("features", features);
					return carJson;
					// return "High-Speed Emergency Braking must be S if Co-Pilot Drive is S";
				}
			}
		}

        return new JSONObject().put("error", "");
	}

}
