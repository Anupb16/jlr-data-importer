package com.jlr.Excel_Importer_module.models;

public class SubFeatures {
	
	private int featureCode;
	private String subFeatureName;
	private String category;
	private String price;
	private String comment;
	
	public SubFeatures() {
		// TODO Auto-generated constructor stub
	}
	
	public SubFeatures(String subFeatureName, String category, String price, String comment, int featureCode) {
		super();
		this.subFeatureName = subFeatureName;
		this.category = category;
		this.price = price;
		this.comment = comment;
		this.featureCode = featureCode;
	}
	
	public String getSubFeatureName() {
		return subFeatureName;
	}
	public void setSubFeatureName(String subFeatureName) {
		this.subFeatureName = subFeatureName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(int featureCode) {
		this.featureCode = featureCode;
	}
}