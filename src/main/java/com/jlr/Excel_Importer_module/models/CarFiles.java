package com.jlr.Excel_Importer_module.models;

import com.blobcity.db.annotations.Entity;

@Entity(ds = "jlr-db")
public class CarFiles {

	private String carId;
	private String fileName;
	
	public CarFiles() {
		// TODO Auto-generated constructor stub
	}
	
	public CarFiles(String carId, String fileName) {
		super();
		this.carId = carId;
		this.fileName = fileName;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	
}
