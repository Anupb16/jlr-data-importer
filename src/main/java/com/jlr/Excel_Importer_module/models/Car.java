package com.jlr.Excel_Importer_module.models;

public class Car {

	private String carName;

	public Car(String carName) {
		super();
		this.carName = carName;
	}
	
	public Car() {
		// TODO Auto-generated constructor stub
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String carName) {
		this.carName = carName;
	}
	
}
