package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;

public class generateCarReportOpt {
	static Map<String, Boolean> masterFeaturesMergeMap = new HashMap<>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		String[] columns = { "New S.No 1.2", "Uniq. Ref. No.", "Feature Group", "NEW CFVS name v1.2", "Discription",
				"Feature Name", "Source" };
		List<JSONObject> carModels = new ArrayList<>();

		List<JSONObject> OrderedMasterFeaturesList = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));

		Collections.sort(OrderedMasterFeaturesList, new Comparator<JSONObject>() {
			@Override
			public int compare(JSONObject o1, JSONObject o2) {
				try {
					return o1.getInt("uniqueSerialNo") - o2.getInt("uniqueSerialNo");
				} catch (JSONException e) {
					e.printStackTrace();
					return 0;
				}
			}
		});

		OrderedMasterFeaturesList.forEach(m -> {
			masterFeaturesMergeMap.put(m.getString("featureCode"), m.getBoolean("merged"));
		});

		JSONObject jsonObject = new JSONObject();
		List<String> list = new ArrayList<>();
		list.add("486f1ce0-adb5-40b4-9027-9b49165aecb3");
		list.add("17563dfc-c7c9-43ec-927b-bbd11050a17f");
		list.add("5b043550-70a2-4e7e-8d10-0d3a924854da");
		jsonObject.put("cid", list);

		JSONArray jsonArray = (JSONArray) jsonObject.get("cid");

		for (int i = 0; i < jsonArray.length(); i++) {
			List<JSONObject> cars = new ArrayList<>();
			cars = Db.searchAsJson(Query.select().from("CarModels").where(SearchParam.create("cid").eq(jsonArray.get(i).toString())));
			carModels.addAll(cars);
		}

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Benchmark");
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		RowCounter1 rc = new RowCounter1();
		OrderedMasterFeaturesList.forEach(feature -> {
			Row row = sheet.createRow(rc.getRowNo());
			row.createCell(0).setCellValue(rc.getRowNo());
			row.createCell(1).setCellValue(feature.getString("featureCode"));
			row.createCell(2).setCellValue(feature.getString("featureGroup"));
			row.createCell(3).setCellValue(feature.getString("featureName"));
			row.createCell(4).setCellValue("");
			row.createCell(5).setCellValue(feature.getString("featureName"));
			row.createCell(6).setCellValue("");
			rc.incrementRow();
		});

		ColumnCounter1 col1 = new ColumnCounter1();
		carModels.forEach(carModel -> {
			RowCounter1 rc1 = new RowCounter1();

			OrderedMasterFeaturesList.forEach(masterFeature -> {
				JSONObject featuresJson = carModel.getJSONObject("features");
				String featureCode = masterFeature.getString("featureCode");
				Row row = sheet.getRow(rc1.getRowNo());

				if (!masterFeaturesMergeMap.containsKey(featureCode)) {
					return;
				}
				if (masterFeaturesMergeMap.get(featureCode) == false) {
					row.createCell(col1.getColNo()).setCellValue(featuresJson.getJSONObject(featureCode).get("category").toString());
					row.createCell(col1.getColNo() + 1).setCellValue(featuresJson.getJSONObject(featureCode).get("price").toString());
					row.createCell(col1.getColNo() + 2).setCellValue(featuresJson.getJSONObject(featureCode).get("comment").toString());
					setBackgroundColor(workbook, col1, row);
				} else {
					row.createCell(col1.getColNo()).setCellValue(featuresJson.getJSONObject(featureCode).get("category").toString());
					sheet.addMergedRegion(new CellRangeAddress(rc1.getRowNo(), rc1.getRowNo(), col1.getColNo(), col1.getColNo() + 2));
				}
				rc1.incrementRow();
			});
			col1.incrementCol();
		});

		File myFile = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData/report8.xlsx");
		FileOutputStream fileOut = new FileOutputStream(myFile);

		workbook.write(fileOut);
		fileOut.close();
		workbook.close();

		System.out.println("done..");
	}

	private static void setBackgroundColor(Workbook workbook, ColumnCounter1 col1, Row row) {
		XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();

		if (row.getCell(col1.getColNo()).toString().equals("S")) {
			setCellStyle(style1, IndexedColors.GREEN.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col1.getColNo()).toString().equals("NA")) {
			setCellStyle(style1, IndexedColors.DARK_RED.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col1.getColNo()).toString().equals("O")) {
			setCellStyle(style1, IndexedColors.DARK_YELLOW.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col1.getColNo()).toString().equals("P")) {
			setCellStyle(style1, IndexedColors.BLUE_GREY.getIndex(), FillPatternType.SOLID_FOREGROUND);
		} else if (row.getCell(col1.getColNo()).toString().equals("(O)")) {
			setCellStyle(style1, IndexedColors.WHITE1.getIndex(), FillPatternType.SOLID_FOREGROUND);
		}
		row.getCell(col1.getColNo()).setCellStyle(style1);
	}

	private static void setCellStyle(XSSFCellStyle style1, short colorIndex, FillPatternType solidForeground) {
		style1.setFillForegroundColor(colorIndex);
		style1.setFillPattern(solidForeground);
	}
}

class RowCounter1 {
	public int row = 1;

	public void incrementRow() {
		this.row += 1;
	}

	public int getRowNo() {
		return this.row;
	}
}

class ColumnCounter1 {
	public int col = 7;

	public void incrementCol() {
		this.col += 3;
	}

	public int getColNo() {
		return this.col;
	}
}