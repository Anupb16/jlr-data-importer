package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.SystemOutLogger;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.search.Query;
import com.mashape.unirest.http.Unirest;

public class AppValidator {

	List<String> files = new ArrayList<String>();
	
	@SuppressWarnings({ "unchecked"})
	public void validateFiles(String file) throws Exception { // todo return type boolean

		FileInputStream excelFile = new FileInputStream(new File(file));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);
		int firstRow = datatypeSheet.getFirstRowNum();
		int lastRow = datatypeSheet.getLastRowNum();
		Row row1 = datatypeSheet.getRow(firstRow + 1);
	
		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			datatypeSheet = workbook.getSheetAt(1);

			firstRow = datatypeSheet.getFirstRowNum();
			lastRow = datatypeSheet.getLastRowNum();
			row1 = datatypeSheet.getRow(firstRow + 1);
			if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
				datatypeSheet = workbook.getSheetAt(2);

				firstRow = datatypeSheet.getFirstRowNum();
				lastRow = datatypeSheet.getLastRowNum();
				row1 = datatypeSheet.getRow(firstRow + 1);
			}
		}
		String errorString = "";

		if (lastRow > 579) {
			lastRow = 579;
		} else {
			lastRow = datatypeSheet.getLastRowNum();
		}
		// 1. Validating Sheet number
		if (row1 == null || datatypeSheet.getLastRowNum() < 500) {
			errorString = "File : " + file + " Benchmark Sheet is not at first position.\n";

		} else {
			// 2. Validating Feature Code
			/*Map<Integer, String> map = new HashMap<>();
			List<String> masterFeaturesDb = new ArrayList<>();
			List<String> masterFeaturesExcel = new ArrayList<>();

			List<JSONObject> orderedMasterFeatures = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));// .orderBy(OrderElement.create("uniqueSerialNo",
																													// Order.ASC)));
			for (JSONObject jsonObject : orderedMasterFeatures) {
				map.put(Integer.valueOf(jsonObject.getString("uniqueSerialNo")), jsonObject.getString("featureCode"));
			}
			Set st = (Set) map.entrySet();
			Iterator it = st.iterator();
			while (it.hasNext()) {
				Map.Entry entry = (Entry) it.next();
				masterFeaturesDb.add(entry.getValue().toString());
			}

			for (int rn = 1; rn <= lastRow; rn++) {
				Row row = datatypeSheet.getRow(rn);
				masterFeaturesExcel.add(row.getCell(1).toString().trim());
			}
			if (!masterFeaturesDb.equals(masterFeaturesExcel)) {
				errorString = errorString + " " + " File: " + file + " Feature code does not match\n";
			}*/

			int i = 0;
			int featureNamestart = 5;
			Row row0 = datatypeSheet.getRow(3);

			if (row0.getCell(1) != null) {
				if (row0.getCell(1).toString().trim().equalsIgnoreCase("S")
						|| row0.getCell(1).toString().trim().equalsIgnoreCase("O")
						|| row0.getCell(1).toString().trim().equalsIgnoreCase("(O)")
						|| row0.getCell(1).toString().trim().equalsIgnoreCase("P")
						|| row0.getCell(1).toString().trim().equalsIgnoreCase("NA")) {
					featureNamestart = 4;
				}
			}

			if (row0.getCell(3).toString() != null) {
				if (row0.getCell(3).toString().trim().equalsIgnoreCase("Model - Identifier")
						|| row0.getCell(3).toString().trim().equalsIgnoreCase("S")
						|| row0.getCell(3).toString().trim().equalsIgnoreCase("O")
						|| row0.getCell(3).toString().trim().equalsIgnoreCase("(O)")
						|| row0.getCell(3).toString().trim().equalsIgnoreCase("P")
						|| row0.getCell(3).toString().trim().equalsIgnoreCase("NA")) {
					featureNamestart = 6;
				}
			}

			if (row1.getCell(5) != null && i == 0) {
				if (row1.getCell(5).toString().trim() != ""
						&& !row1.getCell(5).toString().trim().equalsIgnoreCase("Market")) {
					i = 5;
				}
			}
			if (row1.getCell(6) != null && i == 0) {
				if (row1.getCell(6).toString().trim() != ""
						&& !row1.getCell(6).toString().trim().equalsIgnoreCase("Market")) {
					i = 6;
				}
			}
			if (row1.getCell(7) != null && i == 0) {
				if (row1.getCell(7).toString().trim() != ""
						&& !row1.getCell(7).toString().trim().equalsIgnoreCase("Market")) {
					i = 7;
				}
			}
			if (row1.getCell(8) != null && i == 0) {
				if (row1.getCell(8).toString().trim() != ""
						&& !row1.getCell(8).toString().trim().equalsIgnoreCase("Market")) {
					i = 8;
				}
			}
			if (row1.getCell(9) != null && i == 0) {
				if (row1.getCell(9).toString().trim() != ""
						&& !row1.getCell(9).toString().trim().equalsIgnoreCase("Market")) {
					i = 9;
				}
			}

			// 3. Validating Markets
			List<JSONObject> DbMarkets = Db.searchAsJson(Query.select().from("MasterMarkets"));
			List<String> dbMarkets = new ArrayList<>();
			for (JSONObject jsonObject : DbMarkets) {
				dbMarkets.add(jsonObject.getString("market"));
			}
			if (row1.getCell(featureNamestart) != null) {
				if (row1.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Market")) {
					int k = i;

					while (k < row1.getLastCellNum()) {
						if ((String.valueOf(row1.getCell(k)) != "") && row1.getCell(k) != null) {
							if (dbMarkets.contains(row1.getCell(k).toString().trim())) {
							} else {
								errorString = errorString + " " + " File: " + file + " Market : "
										+ row1.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
							}
						} else {
							//errorString = errorString + " " + " File: " + file + " Market : "
							//		+ row1.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
						}
						if (row1.getCell(k + 3) == null || row1.getCell(k + 3).toString() == "") {
							k = k + 4;
						} else {
							k = k + 3;
						}
					}
				}
			}

			// 4. Validating Manufacturer
			Row row2 = datatypeSheet.getRow(2);
			List<JSONObject> masterManufacturers = Db.searchAsJson(Query.select().from("MasterManufacturers"));
			List<String> dbManufacturers = new ArrayList<>();
			for (JSONObject jsonObject : masterManufacturers) {
				dbManufacturers.add(jsonObject.getString("manufacturer"));
			}
			if (row2.getCell(featureNamestart) != null) {
				if (row2.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Manufacturer")) {
					int k = i;

					while (k < row1.getLastCellNum()) {

						if ((String.valueOf(row2.getCell(k)) != "") && row2.getCell(k) != null) {

							if (dbManufacturers.contains(row2.getCell(k).toString().trim())) {
								if(row2.getCell(k).toString().trim().equals("Land Rover") || row2.getCell(k).toString().trim().equals("Jaguar")) { // TODO remove later
									errorString = errorString + " " + " File: " + file + " Manufacturer : "
											+ row2.getCell(k).toString().trim() + " at column " + k + " do not Add\n";
								}
							} else {
								errorString = errorString + " " + " File: " + file + " Manufacturer : "
										+ row2.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
							}
						} else {
							//errorString = errorString + " " + " File: " + file + " Manufacturer : "
							//		+ row2.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
						}
						if (row1.getCell(k + 3) == null || row1.getCell(k + 3).toString() == "") {
							k = k + 4;

						} else {
							k = k + 3;
						}
					}
				}
			}

			// 5. Validating Models
			Row row3 = datatypeSheet.getRow(3);
			List<JSONObject> masterModels = Db.searchAsJson(Query.select().from("MasterModels"));
			List<String> dbModels = new ArrayList<>();
			for (JSONObject jsonObject : masterModels) {
				dbModels.add(jsonObject.getString("model"));
			}
			if (row3.getCell(featureNamestart) != null) {
				if (row3.getCell(featureNamestart).toString().trim().equalsIgnoreCase("Model")) {
					int k = i;
					while (k < row1.getLastCellNum()) {
						if ((String.valueOf(row3.getCell(k)) != "") && row3.getCell(k) != null) {
							if (dbModels.contains(row3.getCell(k).toString().trim())) {
							} else {
								if (row3.getCell(k).toString().trim().equals("E-pace")
										|| row3.getCell(k).toString().trim().equals("E-Pace")
										|| row3.getCell(k).toString().trim().equals("I-Pace")
										|| row3.getCell(k).toString().trim().equals("F-Pace")
										|| row3.getCell(k).toString().trim().equals("5 series")
										|| row3.getCell(k).toString().trim().equals("S-Class")
										|| row3.getCell(k).toString().trim().equals("Land Cruiser")
										|| row3.getCell(k).toString().trim().equals("911.0")) {
								} else {
									errorString = errorString + " " + " File: " + file + " Model : "
											+ row3.getCell(k).toString().trim() + " at column " + k
											+ " does not exist\n";
								}
							}
						} else {
							//errorString = errorString + " " + " File: " + file + " Model : "
							//		+ row3.getCell(k).toString().trim() + " at column " + k + " does not exist\n";
						}
						if (row1.getCell(k + 3) == null || row1.getCell(k + 3).toString() == "") {
							k = k + 4;
						} else {
							k = k + 3;
						}
					}
				}
			}
		}
		workbook.close();
		if (!errorString.equals("")) {
			sendEmail(errorString);
			//return false; //TODO
		} else {
			//files.addAll(file);
			//return true;
		}
	}

	private void sendEmail(String errorString) {
		System.out.println("Email...." + errorString);
		System.out.println(" ");

	}
	
//	String emailRequest = "{\"template_id\":\"29f2d897-7dd1-4fe9-a2f4-ea28d67ab02d\",\"personalizations\":[{\"to\":[{\"email\":\""
//	//	+ user.get("email").toString()
//		+ "trupti.mane@blobcity.com"
//		+ "\"}],\"subject\":\"Verify your email\"}],\"from\"\"email\":\"noreply@blobcity.com\",\"name\":\"BlobCity (noreply)\"},"
//		+ "\"reply_to\"\"email\":\"noreply@blobcity.com\",\"name\":\"BlobCity (noreply)\"},\"content\":[{\"type\":\"text/plain\","
//		+ "\"value\":\"http://jlr.blobcity.net/login?token="
//		+ emailVerify.get("token").toString()
//		+ "\"},{\"type\":\"text/html\",\"value\":\"http://jlr.blobcity.net/login?token="
//		+ emailVerify.get("token").toString() + "\"}]}";
//
//HttpClient httpclient = HttpClients.createDefault();
//HttpPost postRequest = new HttpPost("https://api.sendgrid.com/v3/mail/send");
//StringEntity input = new StringEntity(emailRequest);
//input.setContentType("application/json");
//postRequest.setEntity(input);
//postRequest.setHeader("authorization",
//		"Bearer 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");

//HttpResponse response = httpclient.execute(postRequest);


//HttpResponse response = Unirest.post(“https://api.sendgrid.com/v3/mail/send“).header(“authorization”, “Bearer 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").header(“content-type”, “application/json”).body(“{\“template_id\“:\“29f2d897-7dd1-4fe9-a2f4-ea28d67ab02d\“,\“personalizations\“:[{\“to\“:[{\“email\“:\“trupti.mane@blobcity.com\“}],\“subject\“:\“Hello, World!\“}],\“from\“:{\“email\“:\“noreply@blobcity.com\“,\“name\“:\“BlobCity (noreply)\“},\“reply_to\“:{\“email\“:\“noreply@blobcity.com\“,\“name\“:\“BlobCity (noreply)\“},\“content\“:[{\“type\“:\“text/plain\“,\“value\“:\“http://jlr.blobcity.net/verification?token=\“”+emailVerify.get(“emailToken”).toString()+“},{\“type\“:\“text/html\“,\“value\“:\“http://jlr.blobcity.net/verification?token=\“”+emailVerify.get(“emailToken”).toString()+“}]}“).asString();
//HttpResponse response = Unirest.post("https://api.sendgrid.com/v3/mail/send").header("authorization", "Bearer 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").header("content-type", "application/json").body("{\"template_id\":\"29f2d897-7dd1-4fe9-a2f4-ea28d67ab02d\",\"personalizations\":[{\"to\":[{\"email\":\"trupti.mane@blobcity.com\"}],\"subject\":\"Hello, World!\"}],\"from\":{\"email\":\"noreply@blobcity.com\",\"name\":\"BlobCity (noreply)\"},\"reply_to\":{\"email\":\"noreply@blobcity.com\",\"name\":\"BlobCity (noreply)\"},\"content\":[{\"type\":\"text/plain\",\"value\":\"http://jlr.blobcity.net/verification?token=\""+emailVerify.get("emailToken").toString()+"},{\"type\":\"text/html\",\"value\":\"http://jlr.blobcity.net/verification?token=\""+emailVerify.get("emailToken").toString()+"}]}").asString();    

}