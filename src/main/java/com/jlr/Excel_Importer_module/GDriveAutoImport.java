package com.jlr.Excel_Importer_module;

import com.blobcity.db.config.Credentials;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.Change;
import com.google.api.services.drive.model.ChangeList;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.StartPageToken;
import com.jlr.cat.importer.SingleSheetImporter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanketsarang on 12/10/18.
 */
public class GDriveAutoImport {

    private static final String FILE_STORAGE_PATH = "/mnt/gdrive/";
    private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
    private static final String TOKENS_DIRECTORY_PATH = "tokens";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = new ArrayList<String>(DriveScopes.all());

    public static void main(String[] args) {

        Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

        try {

            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME).build();

            Credential credential = getCredentials(HTTP_TRANSPORT);
            System.out.println(credential.getAccessToken());


            FileList result = service.files().list().setQ("'" + "19JpOW2b_yaVqOCpMrjvFXDQ3hloYeBKO" + "' in parents")
                    .setFields("nextPageToken, files(id, name, modifiedTime, lastModifyingUser)").execute();
            System.out.println("Result got");

            StartPageToken response = service.changes()
                    .getStartPageToken().execute();

            String savedStartPageToken = response.getStartPageToken();

            String pageToken = savedStartPageToken;
            SingleSheetImporter singleSheetImporter = null;

            final SingleSheetImporter ssi = new SingleSheetImporter();
            ssi.init();

            result.getFiles().forEach(file -> {
                try {
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//
////                    service.files().get(file.getId()).executeMediaAndDownloadTo(outputStream);
//                    service.files().export(file.getId(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet").executeMediaAndDownloadTo(byteArrayOutputStream);
////                    outputStream.flush();
//
//                    try(OutputStream outputStream = new FileOutputStream(new File(FILE_STORAGE_PATH + file.getName() + ".xlsx"))) {
//                        byteArrayOutputStream.writeTo(outputStream);
//                    }
                    URL url = new URL("https://www.googleapis.com/drive/v3/files/" + file.getId() + "?alt=media");
                    HttpURLConnection hc = (HttpURLConnection) url.openConnection();
                    System.out.println("https://www.googleapis.com/drive/v3/files/" + file.getId() + "?alt=media");
                    hc.setRequestProperty("Authorization", "Bearer " + credential.getAccessToken());
                    try (BufferedInputStream in = new BufferedInputStream(hc.getInputStream());
                         FileOutputStream fileOutputStream = new FileOutputStream(file.getName() + ".xlsx")) {
                        byte dataBuffer[] = new byte[1024];
                        int bytesRead;
                        while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                            fileOutputStream.write(dataBuffer, 0, bytesRead);
                        }
                    } catch (IOException e) {
                        // handle exception
                        e.printStackTrace();
                    }

                    ssi.importSheet(FILE_STORAGE_PATH + file.getName() + ".xlsx");

                } catch(Exception ex) {
                    ex.printStackTrace();
                    System.out.println("Could not download file");
                }
            });

            System.out.println("Done Importing");

            while(true) {
                ChangeList changes = service.changes().list(savedStartPageToken)
                        .execute();

                if (!changes.getChanges().isEmpty()) {
                    savedStartPageToken = service.changes().getStartPageToken().execute().getStartPageToken();
                }

                for (Change change : changes.getChanges()) {
                    // Process change



                    System.out.println("Change found for file: " + change.getFileId());
                    System.out.println(change.getFile().getName() + " " + change.getFileId());

                    if(singleSheetImporter == null) {
                        singleSheetImporter = new SingleSheetImporter();
                        singleSheetImporter.init();
                    }

                    try {
                        OutputStream outputStream = new FileOutputStream(
                                FILE_STORAGE_PATH + change.getFile().getName() + change.getFile().getFullFileExtension());

                        service.files().get(change.getFileId()).executeMediaAndDownloadTo(outputStream);
                        outputStream.flush();

                        singleSheetImporter.importSheet(FILE_STORAGE_PATH + change.getFile().getName());

                    } catch(Exception ex) {
                        System.out.println("Could not download file");
                    }


//                    service.files().get(change.getFileId()).

//                    FileList files = service.files().list().setQ("'" + change.getFileId() + "' in 19JpOW2b_yaVqOCpMrjvFXDQ3hloYeBKO")
//                            .setFields("nextPageToken, files(id, name, modifiedTime, lastModifyingUser)").execute();
////                    service.files().get(change.getFileId()).
//                    files.getFiles().forEach(file -> System.out.println(file.getName()));



//                    if(change.getFile().getParents() != null && change.getFile().getParents().isEmpty()) {
//                        change.getFile().getParents().forEach(parent -> System.out.println(parent));
//                    }
                }

                singleSheetImporter = null;

                System.out.println("Will sleep now");
                Thread.sleep(15000);
            }
        }   catch(IOException | GeneralSecurityException ex) {
            ex.printStackTrace();
        } catch(InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        InputStream in = new FileInputStream(
                new java.io.File("/mnt/gdrive/credentials.json"));
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                clientSecrets, SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline").build();
        try {
            return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
        } catch (Exception e) {
            System.out.println("Authorisation failed");
            e.printStackTrace();
            return null;
        }
    }
}
