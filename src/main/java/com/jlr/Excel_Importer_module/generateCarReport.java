package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;
import com.blobcity.db.search.SearchParam;

public class generateCarReport {
	static int count = 0;
	List<String> files = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		String[] columns = { "New S.No 1.2", "Uniq. Ref. No.", "Feature Group", "NEW CFVS name v1.2", "Discription",
				"Feature Name", "Source" };

		Map<Integer, String[]> orderedMasterFeatures = new HashMap<Integer, String[]>();

		List<JSONObject> OrderedMasterFeaturesList = Db.searchAsJson(Query.select().from("OrderedMasterFeatures"));
		
		for (JSONObject jsonObject : OrderedMasterFeaturesList) {
			orderedMasterFeatures.put(Integer.parseInt(jsonObject.get("uniqueSerialNo").toString()), new String[3]);
			orderedMasterFeatures.get(Integer.parseInt(jsonObject.get("uniqueSerialNo").toString()))[0] = jsonObject
					.get("featureCode").toString();
			orderedMasterFeatures.get(Integer.parseInt(jsonObject.get("uniqueSerialNo").toString()))[1] = jsonObject
					.get("featureGroup").toString();
			orderedMasterFeatures.get(Integer.parseInt(jsonObject.get("uniqueSerialNo").toString()))[2] = jsonObject
					.get("featureName").toString();
		}

		JSONObject j = new JSONObject();
		List<String> list = new ArrayList<>();

		list.add("1ac95fc5-c293-4bd5-815f-09370027e323"); //CarModels
		//list.add("51d15ff4-158a-404c-b558-8067aa6c6ddf"); //TestModels
		//list.add("ced07512-5116-48b6-876c-a500d90a975a"); //TestModels
		
		j.put("cid", list);

		JSONArray k = (JSONArray) j.get("cid");

		List<JSONObject> carModels = new ArrayList<>();
		for (int i = 0; i < k.length(); i++) {
			List<JSONObject> cars = new ArrayList<>();
			cars = Db.searchAsJson(
					Query.select().from("CarModels").where(SearchParam.create("cid").eq(k.get(i).toString())));
			carModels.addAll(cars);
		}

		

		Workbook workbook = new XSSFWorkbook();
		Sheet sheet = workbook.createSheet("Benchmark");
		Row headerRow = sheet.createRow(0);
		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		for (int col = 0; col <= 6; col++) {
			for (int rn = 1; rn <= OrderedMasterFeaturesList.size(); rn++) { // TODO 580 534

				Row row;
				if (col == 0) {
					row = sheet.createRow(rn);
				} else {
					row = sheet.getRow(rn);
				}

				if (col == 0)
					row.createCell(col).setCellValue(rn);
				if (col == 1) {
					if (orderedMasterFeatures.get(rn) != null) {
						row.createCell(col).setCellValue(orderedMasterFeatures.get(rn)[0].toString());
					}
				}
				if (col == 2)
					row.createCell(col).setCellValue(orderedMasterFeatures.get(rn)[1].toString());
				if (col == 3)
					row.createCell(col).setCellValue(orderedMasterFeatures.get(rn)[2].toString());
				if (col == 4)
					row.createCell(col).setCellValue("");
				if (col == 5)
					row.createCell(col).setCellValue(orderedMasterFeatures.get(rn)[2].toString());
				if (col == 6)
					row.createCell(col).setCellValue("");
			}
		}

		carModels.forEach(carModel -> {
			try {
				Map<String, String[]> saubFeatureDetails = new HashMap<>();
				Map<String, Object> carModelsDetails = new HashMap<>();
				
				JSONObject featureObject = carModel.getJSONObject("features");
				for (int i = 0; i < featureObject.length(); i++) {
					int z = 0;
					if(Integer.parseInt(featureObject.names().get(i).toString()) > 0) {   //TODO Remove
						JSONObject obj = featureObject.getJSONObject(featureObject.names().get(i).toString());

						saubFeatureDetails.put(featureObject.names().get(i).toString(), new String[3]);

						saubFeatureDetails.get(featureObject.names().get(i).toString())[z++] = obj.getString("price");
						saubFeatureDetails.get(featureObject.names().get(i).toString())[z++] = obj.getString("comment");
						saubFeatureDetails.get(featureObject.names().get(i).toString())[z++] = obj.getString("category");
					}
				}

//				carModelsDetails.put("InfoComments", carModel.getString("Comments"));
//				carModelsDetails.put("Comments", carModel.getString("Comments"));
//				carModelsDetails.put("version", carModel.getString("version"));
				carModelsDetails.put("InfoComments", "");
				carModelsDetails.put("Comments", "");
				carModelsDetails.put("version", "");
				
				carModelsDetails.put("market", carModel.getString("market"));
				carModelsDetails.put("manufacturer", carModel.getString("manufacturer"));
				carModelsDetails.put("model", carModel.getString("model"));
				carModelsDetails.put("trim", carModel.getString("trim"));
				carModelsDetails.put("modelYear", carModel.getString("modelYear"));
				carModelsDetails.put("engineCapacity", carModel.getString("engineCapacity"));
				carModelsDetails.put("power", carModel.getString("power"));
				carModelsDetails.put("fuel", carModel.getString("fuel"));
				carModelsDetails.put("modelCode", carModel.getString("modelCode"));
				carModelsDetails.put("bodyType", carModel.getString("bodyType"));
				carModelsDetails.put("price", carModel.getString("price"));

				for (int col = 6 + (3 * count) + 1; col <= 6 + (count * 3) + 3; col++) {
					for (int rn = 1; rn <= OrderedMasterFeaturesList.size(); rn++) { // TODO 580 495
						Row row;
						row = sheet.getRow(rn);
						if (col == 7 + (3 * count)) {
							if ((rn >= 1 && rn <= 13) || (rn == 24)) {
								if (rn == 1)
									//row.createCell(col).setCellValue(carModelsDetails.get("InfoComments").toString());
								if (rn == 2)
									//row.createCell(col).setCellValue(carModelsDetails.get("Comments").toString());
								if (rn == 3)
									//row.createCell(col).setCellValue(carModelsDetails.get("version").toString());
								if (rn == 4)
									row.createCell(col).setCellValue(carModelsDetails.get("market").toString());
								if (rn == 5)
									row.createCell(col).setCellValue(carModelsDetails.get("manufacturer").toString());
								if (rn == 6)
									row.createCell(col).setCellValue(carModelsDetails.get("model").toString());
								if (rn == 7)
									row.createCell(col).setCellValue(carModelsDetails.get("trim").toString());
								if (rn == 8)
									row.createCell(col).setCellValue(carModelsDetails.get("modelYear").toString());
								if (rn == 9)
									row.createCell(col).setCellValue(carModelsDetails.get("engineCapacity").toString());
								if (rn == 10)
									row.createCell(col).setCellValue(carModelsDetails.get("power").toString());
								if (rn == 11)
									row.createCell(col).setCellValue(carModelsDetails.get("fuel").toString());
								if (rn == 12)
									row.createCell(col).setCellValue(carModelsDetails.get("modelCode").toString());
								if (rn == 13)
									row.createCell(col).setCellValue(carModelsDetails.get("price").toString());
								if (rn == 24)
									row.createCell(col).setCellValue(carModelsDetails.get("bodyType").toString());
								sheet.addMergedRegion(new CellRangeAddress(rn, rn, col, col + 2));
							} else if (rn <= 40) {
								String featureCode = orderedMasterFeatures.get(rn)[0];
								if (saubFeatureDetails.get(featureCode) != null) {
									if (!saubFeatureDetails.get(featureCode)[2].equals("")) {
										row.createCell(col)
												.setCellValue(saubFeatureDetails.get(featureCode)[2].toString()); // Category
									} else {
										row.createCell(col).setCellValue(""); // Category
									}
								} else {
									row.createCell(col).setCellValue(""); // Category
								}
								sheet.addMergedRegion(new CellRangeAddress(rn, rn, col, col + 2));
							} else {

								String featureCode = orderedMasterFeatures.get(rn)[0];

								if (saubFeatureDetails.get(featureCode) != null) {
									if (!saubFeatureDetails.get(featureCode)[2].equals("")) {
										row.createCell(col)
												.setCellValue(saubFeatureDetails.get(featureCode)[2].toString()); // Category
										XSSFCellStyle style1 = (XSSFCellStyle) workbook.createCellStyle();
										
										if (row.getCell(col).toString().equals("S")) {
											style1.setFillForegroundColor(IndexedColors.GREEN.getIndex()); // FInal
											style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
											row.getCell(col).setCellStyle(style1);

										} else if (row.getCell(col).toString().equals("NA")) {
											style1.setFillForegroundColor(IndexedColors.DARK_RED.getIndex()); // FInal
											style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
											row.getCell(col).setCellStyle(style1);
										} else if (row.getCell(col).toString().equals("O")) {
											style1.setFillForegroundColor(IndexedColors.DARK_YELLOW.getIndex()); // FInal
											style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
											row.getCell(col).setCellStyle(style1);
										} else if (row.getCell(col).toString().equals("P")) {
											style1.setFillForegroundColor(IndexedColors.BLUE_GREY.getIndex()); // FInal
											style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
											row.getCell(col).setCellStyle(style1);
										} else if (row.getCell(col).toString().equals("(O)")) {
											style1.setFillForegroundColor(IndexedColors.WHITE1.getIndex()); // FInal
											style1.setFillPattern(FillPatternType.SOLID_FOREGROUND);
											row.getCell(col).setCellStyle(style1);
										}
									} else {
										row.createCell(col).setCellValue(""); // Category
									}
								} else {
									row.createCell(col).setCellValue(""); // Category
								}
							}
						}
						if (col == 8 + (3 * count)) {
							if (rn >= 1 && rn <= 40) {
							} else {
								String featureCode = orderedMasterFeatures.get(rn)[0];
								if (saubFeatureDetails.get(featureCode) != null) {
									if (!saubFeatureDetails.get(featureCode)[0].equals("")) {
										row.createCell(col)
												.setCellValue(saubFeatureDetails.get(featureCode)[0].toString()); // Price
									} else {
										row.createCell(col).setCellValue(""); // Category
									}
								} else {
									row.createCell(col).setCellValue(""); // Category
								}
							}
						}
						if (col == 9 + (3 * count)) {
							if (rn >= 1 && rn <= 40) {
							} else {
								String featureCode = orderedMasterFeatures.get(rn)[0];
								if (saubFeatureDetails.get(featureCode) != null) {
									if (!saubFeatureDetails.get(featureCode)[1].equals("")) {
										row.createCell(col)
												.setCellValue(saubFeatureDetails.get(featureCode)[1].toString()); // Comment
									} else {
										row.createCell(col).setCellValue(""); // Category
									}
								} else {
									row.createCell(col).setCellValue(""); // Category
								}
							}
						}
					}
				}
				count++;
			} catch (Exception e) {
				e.printStackTrace();
			}
		});

		File myFile = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData/report7.xlsx");
		FileOutputStream fileOut = new FileOutputStream(myFile);
		
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();

		System.out.println("done..");
	}
}