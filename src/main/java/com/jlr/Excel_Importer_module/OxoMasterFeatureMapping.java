package com.jlr.Excel_Importer_module;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jlr.Excel_Importer_module.models.OxoMasterFeatureMappingModel;

public class OxoMasterFeatureMapping {

	List<String> files = new ArrayList<String>();

	public static void main(String[] args) throws Exception {

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<String> filePaths = new ArrayList<String>();

		File folder = new File("/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData");

		OxoMasterFeatureMapping listFiles = new OxoMasterFeatureMapping();

		filePaths = listFiles.listAllFiles(folder);
		for (String file : filePaths) {
			processFile(file);
		}
	}

	public List<String> listAllFiles(File folder) {

		File[] fileNames = folder.listFiles();
		for (File file : fileNames) {
			if (file.isDirectory()) {
				listAllFiles(file);
			} else {
				try {
					files.addAll(readContent(file));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return files;
	}

	public List<String> readContent(File file) throws IOException {
		List<String> files = new ArrayList<String>();

		if (file.getCanonicalPath().contains("Archive") || file.getCanonicalPath().contains("archive")) {
		} else if (file.getCanonicalPath().contains(".DS_Store")) {
		} else {
			files.add(file.getCanonicalPath());
		}
		return files;
	}

	public static void processFile(String filePath) throws Exception {

		OxoMasterFeatureMappingModel featureMatrix = null;

		FileInputStream excelFile = new FileInputStream(new File(filePath));
		Workbook workbook = new XSSFWorkbook(excelFile);
		Sheet datatypeSheet = workbook.getSheetAt(0);
		int lastRow = datatypeSheet.getLastRowNum();
		Row row0 = datatypeSheet.getRow(0);

		List<JsonObject> OxoMasterList = new ArrayList<>();
		for (int rn = 1; rn <= lastRow; rn++) {
			Row row = datatypeSheet.getRow(rn);
			String br = "";
			featureMatrix = new OxoMasterFeatureMappingModel();
			for (int col = 0; col < row0.getLastCellNum(); col++) {

				System.out.println("a........   " + row0.getCell(col).toString().trim());
				if (col == 0) {
					featureMatrix.setRadsId(row.getCell(col).toString().trim());

				} else if (col == 1) {
					featureMatrix.setFeatureGroup(row.getCell(col).toString().trim());

				} else if (col == 5) {
					featureMatrix.setSystemDescription(row.getCell(col).toString().trim());

				} else if (col == 6) {
					featureMatrix.setJaguarDescriptions(row.getCell(col).toString().trim());

				} else if (col == 7) {
					featureMatrix.setLandRoverDescription(row.getCell(col).toString().trim());

				} else if (col == 14) {
					featureMatrix.setApplicability(row.getCell(col).toString().trim());

				} else if (col ==15) {
					br = row.getCell(col).toString().trim();
				} else if (col == 16) {
					br = br + row.getCell(col).toString().trim();

				} else if (col == 17) {
					br = br + row.getCell(col).toString().trim();
					if (br.length() > 3) {
						if(!br.contains("N")) {
							if (!br.contains(",")) {
								String s = "";
								int count = 1;
								for (int i = 0; i < br.length(); i++) {
									if (count % 4 != 0) {
										s = s + br.trim().toString().charAt(i);
									} else {
										s = s.concat(" ");
										i--;
									}
									count++;
								}
								String strarray[] = s.split(" ");
								featureMatrix.setExactCodes(strarray);
							} else {
								String s = br.trim().toString();
								String strarray[] = s.split(",");
								featureMatrix.setExactCodes(strarray);
							}
						} else {
							String strarray[] = {};
							featureMatrix.setExactCodes(strarray);
						}			
					} else {
						if (br.equals("N") || br.equals("NN")) {
							String strarray[] = {};
							featureMatrix.setExactCodes(strarray);
						} else {
							String strarray[] = br.split(",");
							featureMatrix.setExactCodes(strarray);
						}
					}
				}
			}
			JSONObject json = new JSONObject(featureMatrix);
			if (!row.getCell(1).toString().trim().equals("")) {
				JsonParser jsonParser = new JsonParser();
				JsonObject oxoMasterJson = (JsonObject) jsonParser.parse(json.toString());
				OxoMasterList.add(oxoMasterJson);
			}
		}
		Db.insertJson("OxoMasterFeatureMapping", OxoMasterList);
		System.out.println("done..");
		workbook.close();
	}
}