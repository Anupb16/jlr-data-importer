package com.jlr.Excel_Importer_module.models;

import com.blobcity.db.Db;
import com.blobcity.db.annotations.Entity;

@Entity(ds = "jlr-db")
public class OrderedMasterFeatures extends Db {

	private String featureCode;
	private String featureGroup;
	private String featureName;
	private String uniqueSerialNo;
	private Boolean specAdjustable;
	private Boolean merged;
	private Boolean visible;

	public OrderedMasterFeatures() {
	}

	public OrderedMasterFeatures(String featureCode, String featureGroup, String featureName, String uniqueSerialNo, Boolean specAdjustable) {
		super();
		this.featureCode = featureCode;
		this.featureGroup = featureGroup;
		this.featureName = featureName;
		this.uniqueSerialNo = uniqueSerialNo;
		this.specAdjustable = specAdjustable;
	}

	public String getFeatureCode() {
		return featureCode;
	}

	public void setFeatureCode(String featureCode) {
		this.featureCode = featureCode;
	}

	public String getFeatureGroup() {
		return featureGroup;
	}

	public void setFeatureGroup(String featureGroup) {
		this.featureGroup = featureGroup;
	}

	public String getFeatureName() {
		return featureName;
	}

	public void setFeatureName(String featureName) {
		this.featureName = featureName;
	}

	public String getUniqueSerialNo() {
		return uniqueSerialNo;
	}

	public void setUniqueSerialNo(String uniqueSerialNo) {
		this.uniqueSerialNo = uniqueSerialNo;
	}

	public Boolean getSpecAdjustable() {
		return specAdjustable;
	}

	public void setSpecAdjustable(Boolean specAdjustable) {
		this.specAdjustable = specAdjustable;
	}

	public Boolean getMerged() {
		return merged;
	}

	public void setMerged(Boolean merged) {
		this.merged = merged;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}
}