package com.jlr.Excel_Importer_module.models;

import java.util.List;

public class Model {

	private String model;
	private List<Trim> trim;
	
	public Model() {
		// TODO Auto-generated constructor stub
	}

	public Model(String model, List<Trim> trim) {
		super();
		this.model = model;
		this.trim = trim;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public List<Trim> getTrim() {
		return trim;
	}

	public void setTrim(List<Trim> trim) {
		this.trim = trim;
	}

}
