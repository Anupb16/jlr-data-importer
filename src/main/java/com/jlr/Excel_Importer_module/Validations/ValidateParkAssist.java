package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateParkAssist implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");
		
		/*if(features.getJSONObject("503").getString("category").isEmpty()) {
            return "for Co-Pilot Park category cannot be blank";
        }*/

        if(features.getJSONObject("503").getString("category").equals("S")) {
			if(!features.getJSONObject("502").getString("category").equals("NA")) {
				features.getJSONObject("502").put("category", "NA");
				carJson.put("features", features);
				return carJson;
				//	return "Park Assist must be NA if Co-Pilot Park is S";
			}
		}

    return new JSONObject().put("error", "");
	}

}
