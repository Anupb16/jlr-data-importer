package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateLaneDepartureWarning implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("510").getString("category").isEmpty()) {
			return "for Lane Keep Assist category cannot be blank";
		}*/

		if (features.getJSONObject("510").getString("category").equals("S")) {
			if (!features.getJSONObject("509").getString("category").equals("NA")) {
				features.getJSONObject("509").put("category", "NA");
				carJson.put("features", features);
				return carJson;
				//return "Lane Departure Warning should be NA if Lane Keep Assist is S";
			}
		}

		return new JSONObject().put("error", "");
	}

}
