package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateWheelDrive implements Validator{

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("102").getString("category").isEmpty()) {
			return "for Driveline - Four Wheel Drive category cannot be blank";
		}*/

		if (features.getJSONObject("102").getString("category").equals("S")) {
			if (!features.getJSONObject("104").getString("category").equals("NA")
					&& !features.getJSONObject("105").getString("category").equals("NA")) {
				return new JSONObject().put("error", "Rear Wheel Drive RWD and Front Wheel Drive FWD must both be NA if Driveline - Four Wheel Drive is S");
			}
		}

		return new JSONObject().put("error", "");

	}

}
