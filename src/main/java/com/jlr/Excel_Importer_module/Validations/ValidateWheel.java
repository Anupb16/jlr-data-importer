package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanketsarang on 05/11/18.
 */
public class ValidateWheel implements Validator {

    @Override
    public JSONObject validate(final JSONObject carJson) {
        final List<String> errors = new ArrayList<>();

        final JSONObject features = carJson.getJSONObject("features");
        final List<String> categories = new ArrayList<>();
        categories.add(features.getJSONObject("237").getString("category"));
        categories.add(features.getJSONObject("238").getString("category"));
        categories.add(features.getJSONObject("239").getString("category"));
        categories.add(features.getJSONObject("240").getString("category"));
        categories.add(features.getJSONObject("241").getString("category"));
        categories.add(features.getJSONObject("242").getString("category"));
        categories.add(features.getJSONObject("243").getString("category"));
        categories.add(features.getJSONObject("244").getString("category"));

        long standardCount = categories.stream().filter(category -> category.equals("S")).count();

        if(standardCount == 0) {
            errors.add("No Standard wheel found");
        } else if(standardCount > 1) {
            errors.add("More than 1 standard wheel found");
        }

        return new JSONObject().put("error", String.join(" | ", errors));
    }
}
