package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateDaytimeRunningLights implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");
		
//		if(features.getJSONObject("180").getString("category").isEmpty()) {
//            return "for Headlamps - LED Signature DRL category cannot be blank";
//        }

        if(features.getJSONObject("180").getString("category").equals("S")) {
			if(!features.getJSONObject("179").getString("category").equals("NA")) {
				features.getJSONObject("179").put("category", "NA");
				carJson.put("features", features);
				return carJson;
				//return "Daytime Running Lights - DRL must be NA if Headlamps - LED Signature DRL is S";
			}
		}

    return new JSONObject().put("error", "");
	}

}
