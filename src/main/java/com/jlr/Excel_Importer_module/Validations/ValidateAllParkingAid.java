package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateAllParkingAid implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("502").getString("category").isEmpty()) {
			return "for Park Assist category cannot be blank";
		}*/

		if (features.getJSONObject("502").getString("category").equals("S")) {
			if (!features.getJSONObject("499").getString("category").equals("S")
					&& !features.getJSONObject("498").getString("category").equals("S")
					&& !features.getJSONObject("497").getString("category").equals("S")) {
				
				features.getJSONObject("499").put("category", "S");
				features.getJSONObject("498").put("category", "S");
				features.getJSONObject("497").put("category", "S");
				carJson.put("features", features);
				return carJson;
				//return "Parking Aid 360, Front Parking Aid and Rear Parking Aid must all be S if Park Assist is S";
			}
		}

		return new JSONObject().put("error", "");
	}

}
