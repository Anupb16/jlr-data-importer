package com.jlr.Excel_Importer_module;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.JSONObject;

import com.blobcity.db.Db;
import com.blobcity.db.config.Credentials;
import com.blobcity.db.search.Query;

public class generateExcelReport {

	List<String> files = new ArrayList<String>();

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws Exception {

		String[] columns = { "RADS-ID", "Feature Group", "JLR Code", "OA Code", "WERS Code", "System Description",
				"Jaguar Description", "Land Rover Description", "Long Description", "EFG Code", "EFG Type",
				"EFG Description", "Collector Code", "Configuration Group", "Applicability", "exactCodes",
				"exactFeatures", "highCodes", "highFeatures", "lowCodes", "lowFeatures" };

		Credentials.init("159.89.171.7:10111", "root", "root", "jlr-db-sb");

		List<JSONObject> oxoMasters = Db.searchAsJson(Query.select().from("OxoMaster"));

		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet("MFD");
		Row headerRow = sheet.createRow(0);

		for (int i = 0; i < columns.length; i++) {
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(columns[i]);
		}

		int rowNum = 1;
		for (JSONObject JSON : oxoMasters) {
			Row row = sheet.createRow(rowNum++);

			row.createCell(0).setCellValue(JSON.get("modelCode").toString());
			row.createCell(1).setCellValue(JSON.get("featureGroup").toString());
			row.createCell(2).setCellValue(JSON.get("jlrCode").toString());
			row.createCell(3).setCellValue(JSON.get("oaCode").toString());
			row.createCell(4).setCellValue(JSON.get("wersCodes").toString());
			row.createCell(5).setCellValue(JSON.get("systemDescription").toString());
			row.createCell(6).setCellValue(JSON.get("jaguarDescriptions").toString());
			row.createCell(7).setCellValue(JSON.get("landRoverDescription").toString());
			row.createCell(8).setCellValue(JSON.get("longDescription").toString());
			row.createCell(9).setCellValue(JSON.get("efgCode").toString());
			row.createCell(10).setCellValue(JSON.get("efgType").toString());
			row.createCell(11).setCellValue(JSON.get("efgDescription").toString());
			row.createCell(12).setCellValue(JSON.get("collectorCode").toString());
			row.createCell(13).setCellValue(JSON.get("configurationGroup").toString());
			row.createCell(14).setCellValue(JSON.get("applicability").toString());
			row.createCell(15).setCellValue(JSON.get("exactCodes").toString());
			row.createCell(16).setCellValue(JSON.get("exactFeatures").toString());
			row.createCell(17).setCellValue(JSON.get("highCodes").toString());
			row.createCell(18).setCellValue(JSON.get("highFeatures").toString());
			row.createCell(19).setCellValue(JSON.get("lowCodes").toString());
			row.createCell(20).setCellValue(JSON.get("lowFeatures").toString());
		}

//		for (int i = 0; i < columns.length; i++) {
//			sheet.autoSizeColumn(i);
//		}

		FileOutputStream fileOut = new FileOutputStream(
				"/Users/trupti/Desktop/TruptiWork/Projects/Jaguar/workspace2/ExcelData/OxoMasterReport.xlsx");
		workbook.write(fileOut);
		fileOut.close();
		workbook.close();
	}
}