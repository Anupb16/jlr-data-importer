package com.jlr.Excel_Importer_module.Validations;

import org.json.JSONObject;

public class ValidateParkingAid implements Validator {

	@Override
	public JSONObject validate(JSONObject carJson) {
		JSONObject features = carJson.getJSONObject("features");

		/*if (features.getJSONObject("503").getString("category").isEmpty()) {
			return "for Co-Pilot Park category cannot be blank";
		}*/

		if (features.getJSONObject("503").getString("category").equals("S")) {
			if (!features.getJSONObject("499").getString("category").equals("S")
					&& !features.getJSONObject("498").getString("category").equals("S")
					&& !features.getJSONObject("497").getString("category").equals("S")) {
				
				features.getJSONObject("499").put("category", "S");
				features.getJSONObject("498").put("category", "S");
				features.getJSONObject("497").put("category", "S");
				carJson.put("features", features);
				return carJson;
				//return "Parking Aid 360, Front Parking Aid and Rear Parking Aid must all be S if Co-Pilot Park is S";
			}
		}

		return new JSONObject().put("error", "");
	}

}
