package com.jlr.Excel_Importer_module.models;

import java.util.List;


public class DbOutput{
	
	private Cars cars;
	private List<Features> features;
	
	public DbOutput() {
		// TODO Auto-generated constructor stub
	}
	
	public DbOutput(Cars cars, List<Features> features) {
		super();
		this.cars = cars;
		this.features = features;
	}
	public Cars getCars() {
		return cars;
	}
	public void setCars(Cars cars) {
		this.cars = cars;
	}
	public List<Features> getFeatures() {
		return features;
	}
	public void setFeatures(List<Features> features) {
		this.features = features;
	}
	
	
	
}
